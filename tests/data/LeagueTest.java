package data;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class LeagueTest {
    Team team = new Team(10, "Jose", "JS");
    Team team2 = new Team(10, "Albert", "AB");
    Match match = new Match(1,5,2,team,team2);
    League league = new League();

    @Test
    void addTeam() {
        league.addTeam(team.name, team.abv);
        
    }

    @Test
    void addMatch() {
    }

    @Test
    void getInstance() {
    }

    @Test
    void testAddTeam() {
    }

    @Test
    void teamList() {
    }

    @Test
    void testAddMatch() {
    }

    @Test
    void matchList() {
    }

    @Test
    void localMatchPoints() {
    }

    @Test
    void globalPoints() {
    }

    @Test
    void globalAverage() {
    }

    @Test
    void victories() {
    }

    @Test
    void empats() {
    }

    @Test
    void maxPoints() {
    }

    @Test
    void minPoints() {
    }

    @Test
    void maxDif() {
    }

    @Test
    void minDif() {
    }

    @Test
    void clasification() {
    }

    @Test
    void teamWin() {
    }

    @Test
    void teamLose() {
    }
}