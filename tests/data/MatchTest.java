package data;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MatchTest {
    Team team1 = new Team(1,"primero","pri");
    Team team2 = new Team(2,"segon","seg");
    Match partit = new Match(1,2,5,team1,team2);

    @Test
    void getWinner() {
        assertEquals(team2,partit.getWinner());
    }

    @Test
    void getPoints1() {
        assertEquals(2,partit.getPoints1());
    }

    @Test
    void setPoints1() {
        partit.setPoints1(7);
        assertEquals(7,partit.getPoints1());
    }

    @Test
    void getPoints2() {
        assertEquals(5,partit.getPoints2());
    }

    @Test
    void setPoints2() {
        partit.setPoints2(19);
        assertEquals(19,partit.getPoints2());
    }
}