package data;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TeamTest {
    @Test
    void getName() {
        Team team = new Team(420,"si","s");
        assertEquals("si",team.getName());
    }

    @Test
    void getId() {
        Team team = new Team(69,"no","n");
        assertEquals("69",team.getId());
    }

    @Test
    void setName() {
        Team team = new Team(10,"adios", "ad");
        team.setName("buenas");
        assertEquals("buenas",team.getName());
    }

    @Test
    void setId() {
        Team team = new Team(10,"adios", "ad");
        team.setId(25);
        assertEquals(25,team.getId());
    }
}
