package ui;

import data.League;
import data.ToFile;

public class MainMenu {
    MyScanner scanner;
    DataMenu dataMenu;
    ClassificationMenu classificationMenu;
    StadisticsMenu statsMenu;
    StadisticsMenu stadisticsMenu ;

    public MainMenu(MyScanner scanner) {
        this.scanner = scanner;
        dataMenu = new DataMenu(scanner);
        classificationMenu = new ClassificationMenu(scanner);
        statsMenu = new StadisticsMenu(scanner);
        stadisticsMenu = new StadisticsMenu(scanner);
    }
    public void showMenu (){
        while(true){
            Printer.printBetweenLines("MENÚ LLIGA ECUAVOLEY 2021");
            System.out.println("Què vols executar?\n");
            System.out.println("0) Exit");
            System.out.println("1) Dades");
            System.out.println("2) Estadístiques");
            System.out.println("3) Classificació");
            int accio = scanner.maxNum(3);
            switch (accio){
                case 3:
                    classificationMenu.clasification();
                    break;
                case 2:
                    stadisticsMenu.estadisticsMenu();
                    break;
                case 1:
                    dataMenu.showMenu();
                    break;
                case 0:
                    ToFile.updateLeagueData(League.getInstance());
                    return;
            }
        }
    }
}