package ui;

import data.League;

public class DataMenu {
    MyScanner scanner;
    League league;
    TeamModifier modifier;
    MatchModifier matchModifier;
    public DataMenu(MyScanner scanner){
        modifier = new TeamModifier(scanner);
        this.scanner = scanner;
        league = League.getInstance();
        matchModifier = new MatchModifier(scanner);
    }

    public void showMenu(){
        while(true) {
            Printer.printBetweenLines("Menu DADES");
            System.out.println("Quina operació vols realitzar?\n");
            System.out.println("1) Introduir equip");
            System.out.println("2) Llista equips");
            System.out.println("3) Modificador d'equips");
            System.out.println("4) Introduir partit");
            System.out.println("5) Llista de partits");
            System.out.println("6) Modificar partit");
            System.out.println("0) Sortir");
            int accio = scanner.maxNum(6);

            switch (accio) {
                case 1:
                    addTeam();
                    break;
                case 2:
                    teamList();
                    break;
                case 3:
                    modifier.ShowMenu();
                    break;
                case 4:
                    addMatch();
                    break;
                case 5:
                    matchList();
                    break;
                case 6:
                    matchModifier.showMenu();
                    break;
                case 0:
                    return;
            }
        }
    }

    private void addTeam(){
        System.out.print("Introdueix el nom de l'equip: \n");
        String name = scanner.next();
        System.out.print("Introdueix l'abreviacio de l'equip: \n");
        String abv = scanner.next();
        league.addTeam(name,abv);
        System.out.printf("\n\u2713\u2713\u2713Equip \"%s\" afegit a la lliga\u2713\u2713\u2713 \n",name);
    }

    private void addMatch(){
        System.out.println("Selecciona els equips: ");
        League.getInstance().teamList();
        int t1 = scanner.maxNum(league.teams.size())-1;
        int t2 = scanner.maxNum(league.teams.size())-1;
        System.out.println("Introdueix les puntuacions de l'equip 1 i 2");
        int p1 = scanner.nextInt();
        int p2 = scanner.nextInt();
        league.addMatch(p1,p2,league.teams.get(t1),league.teams.get(t2));
        System.out.println("\n\u2713\u2713\u2713Partit afegit a la lliga\u2713\u2713\u2713");
    }
    private void teamList(){
        Printer.printBetweenLines("Llista equips");
        league.teamList();
    }

    private void matchList(){
        Printer.printBetweenLines("Llista de partits");
        league.matchList();
    }
}

