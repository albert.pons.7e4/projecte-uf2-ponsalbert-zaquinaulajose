package ui;

/**
 * herramientas esteticas de impresion
 */
public class Printer {
    /**
     * imprime una cadena de caracteres entre dos barras
     * @param string cadena de caracteres a imprimir
     */
    public static void printBetweenLines(String string){
        StringBuilder line= new StringBuilder();
        int lineSize = string.length()+8;
        for (int i = 0; i < lineSize; i++) {
            line.append("-");
        }
        System.out.println(line+"\n"+"    "+string+"\n"+line);
    }
}
