package ui;

import data.League;
import ui.MyScanner;

public class TeamModifier {
    MyScanner scanner;

    public TeamModifier(MyScanner scanner) {
        this.scanner = scanner;
    }

    /**
     * muestra el menu de opciones de modificacion de equipos
     */

    public void ShowMenu(){
        System.out.println("Selecciona l'equip que vols modificar: ");
        League.getInstance().teamList();
        System.out.println("Indica l'ID de l'equip: ");

        int team = scanner.maxNum(League.getInstance().teams.size())-1;
        League.getInstance().teams.get(team);

        System.out.println("1) Modificar nom y abreviació");
        System.out.println("2) Elminar equip");
        int action = scanner.maxNum(2);

        switch (action){
            case 1:
                System.out.print("Introdueix el nou nom de l'equip: \n");
                String name = scanner.next();
                System.out.print("Introdueix la nova abreviació de l'equip: \n");
                String abv = scanner.next();
                League.getInstance().teams.get(team).setAbv(abv);
                break;
            case 2:
                String nameR = League.getInstance().teams.get(team).getName();
                League.getInstance().teams.remove(team);
                System.out.printf("\n\u2713\u2713\u2713Equip %s remogut amb èxit\u2713\u2713\u2713\n",nameR);
                for (int i = 0; i < League.getInstance().teams.size(); i++) {
                    League.getInstance().teams.get(i).setId(i+1);
                }
                break;
        }
    }
}
