package ui;

import data.League;

public class StadisticsMenu {
    MyScanner scanner;
    League league;
    //contructor
    public StadisticsMenu(MyScanner scanner) {
        this.scanner = scanner;
        league=League.getInstance();

    }

    public void estadisticsMenu() {
        while(true) {
            System.out.println(
                    "1) Estadístiques globals\n" + "2) Estadístiques locals\n" +
                            "0) Tornar");
            int input = scanner.maxNum(2);
            switch (input) {
                case 1:
                    globalStadisticsMenu();
                    break;
                case 2:
                    localStadisticsMenu();
                    break;
                case 0:
                    return;
            }
        }
    }

    private void localStadisticsMenu() {
        while (true) {
            System.out.println("1) Nombre total de punts marcats i mitjana de punts per partits d'un equip\n" +
                    "2) Nombre total de victories i empats d'un equip. Punts\n" +
                    "3) Total de punts marcats i punts en contra\n" +
                    "4) Equip contra el que més/menys vegades ha guanyat\n" +
                    "0) Retorna\n");

            System.out.println("introdueix una opció: ");
            int input = scanner.maxNum(4);
            System.out.println("Selecciona el Equip:");
            league.teamList();
            int i = scanner.maxNum(league.match.size()) - 1;
            switch (input) {
                case 1:
                    System.out.printf("Total de punts: %d \nMitja de punts: %.2f\n",
                            league.teams.get(i).getPoints(), league.teams.get(i).pointPerMatch());
                    break;
                case 2:
                    System.out.printf("Victories: %d\nEmpats: %d\n",
                            league.teams.get(i).getVictories(), league.teams.get(i).getTies());
                    break;
                case 3:
                    System.out.printf("Punts Marcats: %d\nPunts en contra: %d\n",
                            league.teams.get(i).getPoints(), league.teams.get(i).negativePoints());
                    break;
                case 4:
                    league.teamList();
                    System.out.println("Escriu el nom de l'equip: ");
                    String name = scanner.next();
                    System.out.println("L'equip: " + name + " Té tantes victories contra aquests equips: " + league.teamWin(name) + " i te tantes derrotes contra aquests equips: "+league.teamLose(name));
                    break;
                case 0:
                    return;
            }
        }
    }

    private void globalStadisticsMenu() {
        System.out.println("1)Nombre total de punts marcats i mitjana de punts per partits.\n" +
                "2)Nombre total de victories i empats.\n" +
                "3)Partit amb més/menys punts.\n" +
                "4) Partit amb més/menys diferència de punts.");
        System.out.println("introdueix una opció: ");
        int input = scanner.maxNum(4);
        switch (input){
            case 1:
                System.out.println("Punts totals: "+(int)League.getInstance().globalPoints()+
                        "\nMitja de Punts: "+ League.getInstance().globalAverage());
                break;
            case 2:
                System.out.println("Victories: "+League.getInstance().victories() +
                        "\nEmpats: "+League.getInstance().empats());
                break;
            case 3:
                System.out.println("Partit amb més punts: "+ league.maxPoints() +
                        "\nPartit amb menys punts: "+league.minPoints() );
                break;
            case 4:
                System.out.println("Partit amb més diferencia de punts: "+league.maxDif()[0] +")"+
                        " "+league.maxDif()[1]+" punts" +"\nPartit amb menys diferencia de punts: "+
                        league.minDif()[0]+") "+league.minDif()[1]+" punts");
                break;
        }

    }
}
