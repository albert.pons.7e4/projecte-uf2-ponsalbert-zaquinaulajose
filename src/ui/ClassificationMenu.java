package ui;

import data.League;

public class ClassificationMenu {
    MyScanner scanner;
    League league;
    //contructor
    public ClassificationMenu(MyScanner scanner) {
        this.scanner = scanner;
        league = League.getInstance();
    }

    public void clasification() {
        System.out.println("Classificació 2020/2021\n" +
                "#EQUIP          PUNTS\n"); //cuantos tabs deberé poner?
        league.clasification();
    }
}
