package ui;

import java.util.Locale;
import java.util.Scanner;

public class MyScanner {
    Scanner scanner;

    public MyScanner() {
        this.scanner = new Scanner(System.in).useLocale(Locale.US);
    }

    public int maxNum(int numMax){
        int accio = scanner.nextInt();
        while(accio < 0 || accio >numMax){
            System.out.println("ERROR");
            accio = scanner.nextInt();
        }
        return accio;
    }
    public String next(){
        return scanner.next();
    }
    public int nextInt(){
        return scanner.nextInt();
    }


    public Scanner getDefaultScanner() {
        return scanner;
    }
}