package ui;

import data.League;
import data.Team;
import ui.MyScanner;

public class MatchModifier {
    MyScanner scanner;
    League league = League.getInstance();
    //contructor

    public MatchModifier(MyScanner scanner) {
        this.scanner = scanner;
    }

    public void showMenu(){
        System.out.println("Selecciona el partit que vols modificar: ");
        league.matchList();
        System.out.print("Indica el partit: ");

        int partit= scanner.maxNum(league.match.size());
        League.getInstance().match.get(partit-1);

        System.out.println("1) Modificar equips");
        System.out.println("2) Modificar punts");
        System.out.println("3) Eliminar partit");
        int action = scanner.maxNum(3);
        System.out.println();
        switch (action){
            case 1:
                modifyEquips(partit);
                break;
            case 2:
                modifyPunts(partit);
                break;
            case 3:
                League.getInstance().match.remove(partit-1);
                System.out.printf("\n\u2713\u2713\u2713Partit %d remogut amb èxit\u2713\u2713\u2713\n",partit);
                for (int i = 0; i < League.getInstance().match.size(); i++) {
                    League.getInstance().match.get(i).setId(i+1);
                }
                break;
        }
    }

    private void modifyPunts(int partit) {
        System.out.println("1) Modificar punts 1");
        System.out.println("2) Modificar punts 2");
        int points = scanner.maxNum(2);
        System.out.print("indica els punts de reemplaçament");
        int newPoints = scanner.nextInt();
        switch (points){
            case 1:
                league.match.get(partit-1).setPoints1(newPoints);
                break;
            case 2:
                league.match.get(partit-1).setPoints2(newPoints);
                break;
        }
    }

    private void modifyEquips(int partit) {
        System.out.println("1) Modificar equip 1");
        System.out.println("2) Modificar equip 2");
        int equip = scanner.maxNum(2);
        league.teamList();
        System.out.println("indica el equip de reemplaçament");
        int IDequip = scanner.maxNum(league.teams.size());
        switch (equip){
            case 1:
                Team newTeam = league.teams.get(IDequip-1);
                league.match.get(partit-1).setTeam1(newTeam);
                break;
            case 2:
                Team newTeam2 = league.teams.get(IDequip-1);
                league.match.get(partit-1).setTeam2(newTeam2);
                break;
        }
    }
}
