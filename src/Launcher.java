import data.FromFile;
import data.League;
import data.Team;
import ui.MainMenu;
import ui.MyScanner;
public class Launcher {

    public static void main(String[] args) {
        MyScanner scanner = new MyScanner();
        MainMenu mainMenu = new MainMenu(scanner);
        //inicializa la clase liga con los datos de los archivos
        FromFile.from();
        //icicia el menu
        mainMenu.showMenu();
    }

}
