package data;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Scanner;

public class FromFile {
    public static void fromTeamFile() {
        Path file = Path.of("files/teamData.txt");

        try {
            Scanner scannerFile = new Scanner(file);
            while (scannerFile.hasNext()) {
                String n = scannerFile.next();
                String m = scannerFile.next();
                League.getInstance().addTeam(n,m);
            }
        } catch (IOException e) {
            System.err.println("wrong route");
        }
    }

    public static void fromMatchFile() {
        Path file = Path.of("files/matchData.txt");

        try {
            Scanner scannerFile = new Scanner(file);
            while (scannerFile.hasNext()) {
                int a = scannerFile.nextInt();
                int b = scannerFile.nextInt();
                int c = scannerFile.nextInt() - 1;
                int d = scannerFile.nextInt() - 1;
                League.getInstance().addMatch(a,b,League.getInstance().teams.get(c),League.getInstance().teams.get(d));
            }
        } catch (IOException e) {
            System.err.println("wrong route");
        }
    }

    public static void from() {
        fromTeamFile();
        fromMatchFile();
    }
}
