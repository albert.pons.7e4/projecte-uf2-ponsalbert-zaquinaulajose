package data;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class ToFile {
    //vars
    public static Path teamPath = Paths.get("./files/teamData.txt");
    public static Path  matchPath = Paths.get("./files/matchData.txt");

    //functions
    public static void toTeamfile(League league){
        //borra el contenido del archivo
        try {
            Files.delete(teamPath);
            Files.createFile(teamPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //añade el contenido de la lista team al archivo
        for (int i = 0; i < league.teams.size(); i++){
            int id = league.teams.get(i).getId();
            String name = league.teams.get(i).getName();
            String abv = league.teams.get(i).getAbv();
            try {
                Files.writeString(teamPath, (name + " " + abv+"\n"), StandardOpenOption.APPEND);
            } catch(IOException e){
                e.printStackTrace();
            }
        }
    }
    public static void toMatchfile(League league){
            //borra contenido del arhivo
        try {
            Files.delete(matchPath);
            Files.createFile(matchPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //añade contenido al archivo de la lista partidos
        for (int i = 0; i < league.match.size(); i++){
            int id = league.match.get(i).getId();
            int idteam1 = league.match.get(i).team1.getId();
            int idteam2 = league.match.get(i).team2.getId();
            int point1 = league.match.get(i).points1;
            int points2 = league.match.get(i).points2;
            try {
                Files.writeString(matchPath, ( point1 + " "+ points2 + " "+idteam1 + " " + idteam2 + "\n"), StandardOpenOption.APPEND);
            } catch(IOException e){
                e.printStackTrace();
            }
        }
    }
    public static void updateLeagueData(League league){
        toMatchfile(league);
        toTeamfile(league);
    }
}
