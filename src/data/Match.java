package data;

public class Match {
    int id;
    int points1;
    int points2;
    Team team1;
    Team team2;
    Team winner;
    Team looser;

    public Match(int id, int points1, int points2, Team team1, Team team2){
        this.id = id;
        this.points1 = points1;
        this.points2 = points2;
        this.team1 = team1;
        this.team2 = team2;
        winner = null;
        looser = null;
        calcWinner();
        updateTeams();
    }
    //function

    private void updateTeams() {
        addPointsToTeams();
        team1.addMatch(this);
        team2.addMatch(this);
    }
    public void calcWinner(){
        if(points1 > points2){
            winner = team1;
            team1.addVictories();
            looser = team2;
            team2.addDefeats();
        }
        else
        if(points1 == points2){
            winner =null;
            team1.addTies();
            team2.addTies();
        }
        else {
            winner = team2;
            team2.addVictories();
            looser = team1;
            team1.addDefeats();
        }
    }

    private void addPointsToTeams() {
        team1.addPoints(points1);
        team2.addPoints(points2);
    }
    //getters and setters
    public Team getWinner() {
        return winner;
    }

    public int getPoints1() {
        return points1;
    }

    public void setPoints1(int points1) {
        this.points1 = points1;
    }

    public int getPoints2() {
        return points2;
    }

    public int getId() {
        return id;
    }

    public void setPoints2(int points2) {
        this.points2 = points2;
    }

    public Team getTeam1() {
        return team1;
    }

    public String gTeam1(){
        return String.valueOf(team1);
    }

    public void setTeam1(Team team1) {
        this.team1 = team1;
    }

    public Team getTeam2() {
        return team2;
    }
    public String gTeam2(){
        return String.valueOf(team2);
    }

    public void setTeam2(Team team2) {
        this.team2 = team2;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return String.format("(%d) %s %d - %d %s\n", id, team1, points1, points2, team2);
    }
}
