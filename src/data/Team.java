package data;

import java.util.ArrayList;
import java.util.List;

public class Team {
    String name;
    String abv;
    int id;
    private int points;
    private List<Match> matches = new ArrayList<Match>();
    private int victories;
    private int ties;
    private int defeats;
    //contructor
    public Team(int id, String name, String abv) {
        this.id = id;
        this.name = name;
        this.abv = abv;
        points =0;
        victories = 0;
        ties = 0;
        defeats = 0;
    }
    //functions
    public void addVictories(){
        victories ++;
    }
    public void addTies(){
        ties++;
    }
    public void addDefeats(){
        defeats ++;
    }
    public void addPoints(int points) {
        this.points += points;
    }
    public void addMatch(Match match){
        matches.add(match);
    }
    public float pointPerMatch(){
        return (float) points / matches.size();
    }

    /**
     * busca en la lista de matches el equipo contrario  de cada match y suma sus puntos
     * @return suma de todos los puntos de los equipos contrarios
     */
    public int negativePoints(){
        int nPoints=0;
        for (int i = 0; i < matches.size(); i++) {
            if(matches.get(i).team1 == this) //averigua cual team es el mismo
                nPoints+= matches.get(i).points2;
            else
                nPoints+= matches.get(i).points1;
        }
        return nPoints;
    }

    //getters and setters

    public String getName() {
        return name;
    }

    public String getAbv() {
        return abv;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAbv(String abv) {
        this.abv = abv;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPoints() {
        return points;
    }

    public int getVictories() {
        return victories;
    }

    public int getTies() {
        return ties;
    }

    @Override
    public String toString() {
        return String.format("%s (%s)", name, abv);
    }
}
