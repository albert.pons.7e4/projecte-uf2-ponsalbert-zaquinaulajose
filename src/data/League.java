package data;

import java.util.*;

public class League {


    //VARs
    private static League league;
    public List<Team> teams;
    public List<Match> match;

    //contructor
    public League() {
        teams = new ArrayList<Team>();
        match = new ArrayList<Match>();
    }

    //functions

    /**
     * metodo singleton
     *
     * @return el objeto league statico
     */
    public static League getInstance() {
        if (league == null)
            league = new League();
        return league;
    }

    public void addTeam(String name, String abv){
        int id = teams.size()+1;
        Team a = new Team(id, name, abv);
        teams.add(a);
    }

    public void teamList(){
        for(Team a : teams)
            System.out.printf("%d %s\n", a.getId(), a);
    }

    public void addMatch(int points1, int points2, Team team1, Team team2){
        int id = match.size()+1;
        Match a = new Match(id, points1, points2, team1, team2);
        match.add(a);
    }

    public void matchList(){
        for(Match a: match)
            System.out.printf("%s\n", a );
    }
    //local functions
    public double localMatchPoints(int i){
        return match.get(i).getPoints1()+match.get(i).getPoints2();
    }

    public double globalPoints(){
        double tPoints=0;
        for (int i = 0; i < match.size(); i++) {
            tPoints+= localMatchPoints(i);
        }
        return tPoints;
    }

    public double globalAverage(){
        double a = globalPoints();
        int b = match.size();
        double  c = a/(double)b;
        return c;
    }

    public int victories(){
        int v = 0;
        for (int i = 0; i < match.size(); i++) {
            if (match.get(i).getPoints1()>match.get(i).getPoints2() || match.get(i).getPoints1()<match.get(i).getPoints2())
                v ++;
        }
        return v;
    }
    public int empats(){
        int e = 0;
        for (int i = 0; i < match.size(); i++) {
            if (match.get(i).getPoints1() == match.get(i).getPoints2())
                e ++;
        }
        return e;
    }
    public double maxPoints(){
        double max = localMatchPoints(0);
        for (int i = 1; i < match.size(); i++) {
            if(max < localMatchPoints(i))
                max = localMatchPoints(i);

        }
        return max;
    }
    public double minPoints(){
        double min = localMatchPoints(0);
        for (int i = 1; i < match.size(); i++) {
            if(min > localMatchPoints(i))
                min = localMatchPoints(i);
        }
        return min;
    }

    public int[] maxDif(){
        List<Integer> list = new ArrayList<Integer>();
        int index=0;
        int[] maxdif = new int[2];
        for (int i = 0; i < match.size(); i++) {
            int a =match.get(i).getPoints1()-match.get(i).getPoints2();
            list.add(a);
        }
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i) == Math.abs(Collections.max(list)))
                index = i;
        }
        maxdif[0]=index;
        maxdif[1] = Math.abs(Collections.max(list));
        return  maxdif;
    }
    public int[] minDif(){
        List<Integer> list = new ArrayList<Integer>();
        int[] mindif = new int[2];
        int index=0;
        for (int i = 0; i < match.size(); i++) {
            int a =match.get(i).getPoints1()-match.get(i).getPoints2();
            list.add(a);
        }
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i) == Math.abs(Collections.min(list)))
                index = i;
        }
        mindif[0]=index;
        mindif[1] = Math.abs(Collections.min(list));
        return  mindif;
    }
    //clasification
    public void clasification(){
        //crea una lista de los euipos en orden de puntos
        int index=0;
        List<Team> clasificacionList = new ArrayList<Team>(teams);
        for (int i = 1; i < clasificacionList.size(); i++) {
            for (int j = i; j < clasificacionList.size(); j++) {
                if(clasificacionList.get(j-1).getPoints()< clasificacionList.get(j).getPoints()){
                    Team min = clasificacionList.get(i-1);
                    clasificacionList.set(0,clasificacionList.get(j));
                    clasificacionList.set(clasificacionList.size()-1,min);
                }
            }
        }
        //imprime la lista
        for (int i = 0; i < clasificacionList.size(); i++) {
            System.out.println(clasificacionList.get(i).name+"       "+
                    clasificacionList.get(i).getPoints());
        }

    }

    public Set<String> teamWin(String name) {
        List<String> list = new ArrayList<String>();
        Set<String> distinct = null;
        for (int i = 0; i < match.size(); i++) {
            if (name.equals(match.get(i).gTeam1()) || name.equals(match.get(i).gTeam2())) {
                if (name.equals(match.get(i).gTeam1()))
                    if (match.get(i).points1 > match.get(i).points2)
                        list.add(match.get(i).gTeam2());
                    else if (name.equals(match.get(i).gTeam2()))
                        if (match.get(i).points1 < match.get(i).points2)
                            list.add(match.get(i).gTeam1());
            }
            distinct = new HashSet<>(list);
            for (String s : distinct) {
                System.out.println(s + ": " + Collections.frequency(list, s));
            }
        }
        return distinct;
    }

    public Set<String> teamLose(String name) {
        List<String> list2 = new ArrayList<String>();
        Set<String> distinct = null;
        for (int i = 0; i < match.size(); i++) {
            if (name.equals(match.get(i).gTeam2()) || name.equals(match.get(i).gTeam1())) {
                if (name.equals(match.get(i).gTeam2()))
                    if (match.get(i).points1 > match.get(i).points2)
                        list2.add(match.get(i).gTeam2());
                    else if (name.equals(match.get(i).gTeam1()))
                        if (match.get(i).points1 < match.get(i).points2)
                            list2.add(match.get(i).gTeam1());
            }
            distinct = new HashSet<>(list2);
            for (String s : distinct) {
                System.out.println(s + ": " + Collections.frequency(list2, s));
            }
        }
        return distinct;
    }

}
