#Projecte UF2
####Integrantes: 
* Albert Pons
* Jose Zaquinaula
####Fecha: 28/02/2021
##Introducción
El proyecto consiste en crear un programa que guarde datos de un campeonato de EcuaVoley.

Ecuavoley es un juego de 6 personas dividas en equipos de 3 jugadores.
El equipo que acumule más puntos al final de la partida ganará.
Al final de una partida hay un ganador, un perdedor o puede haber un empate

La liga es en todo el país y cada equipo se le otorga el nombre de la provincia que representa.

Se usa programacion orientada a objetos para la realizacion del proyecto
con el IDE intellijIDE.

El programa sera capaz de mostrar:
* Datos de un Equipo
* Datos de un Partido
* Datos globales del campeonato

Tambien se podra modificar:
* Datos de un equipo
* Datos de un partido




##Clases
* Launcher
###Paquete ui
#### ClasificationMenu
| Objetos |    Tipo   |          Descripcion         |
|:-------:|:---------:|:----------------------------:|
| scanner | MyScanner | lee las entradas del usuario |
|  league |   League  |        liga de equipos       |

|      Funciones     |  Entradas | Salidas |                                                      Descripcion                                                      |
|:------------------:|:---------:|:-------:|:---------------------------------------------------------------------------------------------------------------------:|
| ClassificationMenu | MyScanner |    --   |                  contructor. inicializa scanner  inicializa liga como el singleton de la clase League                  |
|    clasification   |     --    |   void  | muestra la tabla de clasificación de los equipos del objeto liga llama a la función clasification() del objeto league |
#### DataMenu
|     Objeto    |      Tipo     |          Descripcion         |
|:-------------:|:-------------:|:----------------------------:|
|    scanner    |   MyScanner   | lee las entradas del usuario |
|     league    |     League    |        liga de equipos       |
| modifier      | TeamModifier  | modifica datos de un partido |
| matchModifier | MatchModifier | modifica datos de un equipo  |

|   Funciones  |  Entradas | Salidas |                                               Descripcion                                              |
|:------------:|:---------:|:-------:|:------------------------------------------------------------------------------------------------------:|
|  DataMenu()  | MyScanner |    --   |  Contructor inicializa scanner  liga = singleton de League instancia matchModifier instancia modifier  |
|  showMenu()  |     --    |   void  | Muestra el menu de datos de la liga llama a diferentes metodos segun la opcion escogida por el usuario |
| insertTeam() |     --    |   void  |                           Muestra un menu de opciones y añade un nuevo equipo                          |
|  addMatch()  |     --    |   void  |                          Muestra un menu de opciones y añade un nuevo partido                          |
|  teamList()  |     --    |   void  |                 muestra la lista de equipos llamando a la función teamList() de league                 |
| matchList()  |     --    |   void  |                 Muestra la lista de partidos mediante la funcion matchList() de league                 |
#### MainMenu
|     Objeto    |      Tipo     |          Descripcion         |
|:-------------:|:-------------:|:----------------------------:|
|    scanner    |   MyScanner   | lee las entradas del usuario |
|     league    |     League    |        liga de equipos       |
| classificationMenu| ClassificationMenu  | Accede al menu de clasificación |
| statsMenu | StadisticsMenu | Accede al menu de estadisticas  |

|   Funciones  |  Entradas | Salidas |                                               Descripcion                                              |
|:------------:|:---------:|:-------:|:------------------------------------------------------------------------------------------------------:|
|  mainMenu()  | MyScanner |   void  |  Contructor inicializa scanner  liga = singleton de League instancia matchModifier instancia modifier  |
|  showMenu()  |     --    |   void  | Muestra el menu de datos de la liga llama a diferentes metodos segun la opcion escogida por el usuario |

#### MatchModifier
|     Objeto    |      Tipo     |          Descripcion         |
|:-------------:|:-------------:|:----------------------------:|
|    scanner    |   MyScanner   | lee las entradas del usuario |
|     league    |     League    |        liga de equipos       |

|   Funciones  |  Entradas | Salidas |                                               Descripcion                                              |
|:------------:|:---------:|:-------:|:------------------------------------------------------------------------------------------------------:|
|  showMenu()  |     --    |   void  | Muestra el menu de datos de la liga llama a diferentes metodos segun la opcion escogida por el usuario |
| modifyPunts()|    int    |   void  | Permite modificar los puntos de un partido                          |
#### MyScanner
|     Objeto    |      Tipo     |          Descripcion         |
|:-------------:|:-------------:|:----------------------------:|
|    scanner    |    Scanner    | lee las entradas del usuario |

|   Funciones  |  Entradas | Salidas |                                               Descripcion                                              |
|:------------:|:---------:|:-------:|:------------------------------------------------------------------------------------------------------:|
|   maxNum ()  |     int   |   int   | Un scanner para un numero maximo que se de |
|    next()    |     --    |   void  | Escanea el proximo input                          |
|  nextInt()   |     --    |   void  | Escanea el proximo int                          |
#### Printer
|      Funciones      | Entradas | Salidas |                     Descripcion                     |
|:-------------------:|:--------:|:-------:|:---------------------------------------------------:|
| printBetweenLines() |  String  |   void  | imprime entre el string de entrada lineas paralelas |

####StadisticsMenu

|  Objeto |    Tipo   |          Descripcion         |
|:-------:|:---------:|:----------------------------:|
| scanner | MyScanner | lee las entradas del usuario |
|  league |   League  |        liga de equipos       |

|        Funciones        |  Entradas | Salidas |                                                              Descripcion                                                              |
|:-----------------------:|:---------:|:-------:|:-------------------------------------------------------------------------------------------------------------------------------------:|
|     StadisticsMenu()    | MyScanner |    --   |                                            inicializa scanner league = singleton de League                                            |
|    estadisticsMenu()    |     --    |   void  |                               Muestra un menu de opciones llama a un metodo según la entrada del usuario                              |
|  localEstadisticsMenu() |     --    |  void   | Muestra un menu de opciones Muestra la lista de equipos para que el usuario escoga uno realiza una acción según la opción del usuario |
| globalStadisticsMenu() |     --    |   void  |                               Muestra un menu de opciones Realiza una acción según la opción del usuario                              |
#### TeamModifier
|     Objeto    |      Tipo     |          Descripcion         |
|:-------------:|:-------------:|:----------------------------:|
|    scanner    |   MyScanner   | lee las entradas del usuario |

|   Funciones  |  Entradas | Salidas |                                               Descripcion                                              |
|:------------:|:---------:|:-------:|:------------------------------------------------------------------------------------------------------:|
|  showMenu()  |     --    |   void  | Muestra el menu de datos de la liga llama a diferentes metodos segun la opcion escogida por el usuario |
###Paquete data
#### League
|     Objeto    |      Tipo     |          Descripcion         |
|:-------------:|:-------------:|:----------------------------:|
|    league     |   League      | Llama a la clase liga        |
|     teams     |     List      |        Lista de equipos      |
|   match       | List          | Lista de partidos            |

|   Funciones  |  Entradas | Salidas |                                               Descripcion                                              |
|:------------:|:---------:|:-------:|:------------------------------------------------------------------------------------------------------:|
| getInstance()| --        |    --   | Metodo singletone para llamar a todos los datos y guardarlos  |
|  addTeam()   |    String |   void  | Añade equipo a la lista de equipos |
| teamList()   |     --    |   void  |Printa la lista de equipos                          |
|  addMatch()  |     --    |   void  |                          Muestra un menu de opciones y añade un nuevo partido                          |
|  matchList() |     --    |   void  |                 Printa la lista de partidos                 |
| matchList()  |     --    |   void  |                 Muestra la lista de partidos mediante la funcion matchList() de league                 |
#### Match
|  Objeto | Tipo |          Descripcion          |
|:-------:|:----:|:-----------------------------:|
|    id   |  int | identificador único de equipo |
| points1 | int  |      puntos del equipo 1      |
| points2 | int  |      puntos del equipo 2      |
|  team1  | Team |          equipo local         |
|  team2  | Team |        equipo visitante       |
|  winner | Team |         equipo ganador        |
|  looser | Team |        equipo perdedor        |

|      Funciones     |            Entradas            | Salidas |                                                                                                              Descripcion                                                                                                             |
|:------------------:|:------------------------------:|:-------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
|       Match()      | id,points1,points2,team1,team2 |    --   |                                   inicializa id inicializa points1 inicializa points2 inicializa team1 inicializa team2 winner = null looser = null llama a los metodos: calcWinner() updateTeams()                                  |
|    updateTeams()   |               --               |   void  |                                                                           llama a los métodos:  addPointsToTeams() addMatch() de team1 addMatch() de team2                                                                           |
|    calcWinner()    |               --               |   void  | calcula el equipo ganador y lo coloca en el objeto winner calcula el equipo perdedor y lo coloca en el objeto looser si hay empate el objeto winner es null llama a los metodos de team 1 y 2: addTies() addDefeats() addVictories() |
| addPointsToTeams() |               --               |   void  |                                                                                           llama a los metodos de team1 y team2: addPoints()                                                                                          |
| toString()         | --                             | String  | string con formato: ("(%d) %s %d - %d %s\n",id, team1, points1,points2,team2)                                                                                                                                                        |
#### Team
|   Objeto  |     Tipo    |            Descripcion            |
|:---------:|:-----------:|:---------------------------------:|
|     id    |     int     |   identificador único de equipo   |
|    abv    |    String   | abreviatura del nombre del equipo |
|    name   |    String   |         nombre del equipo         |
|   points  |     int     |     puntos totales del equipo     |
|  matches  | List<Match> |     lista de partidos jugados     |
| victories |     int     |             victorias             |
|    ties   |     int     |              empates              |
| defeats   | int         | derrotas                          |

|     Funciones    |   Entradas  | Salidas |                                    Descripcion                                   |
|:----------------:|:-----------:|:-------:|:--------------------------------------------------------------------------------:|
|      Team()      | id,name,abv |    --   | CONTRUCTOR inicializa: id,name,abv points = 0 victories = 0 ties = 0 defeats = 0 |
|  addVictories()  |      --     |   void  |                                    victories++                                   |
|     addTies()    |      --     |   void  |                                      ties++                                      |
|   addDefeats()   |      --     |   void  |                                     defeats++                                    |
| addPoints()      | int points  | void    | suma la entrada points al objeto points                                          |
| addMatch()       | Match match | void    | añade un partido en la lista de partidos                                         |
| pointsPerMatch() | --          | float   | calcula la media de puntos por partido                                           |
| negativePoints() | --          | int     | calcula los puntos en contra                                                     |
| toString()       | --          | String  | devuelve un string con el nombre y la abreviacion del equipo                     |

#### FromFile
|   Objeto  |     Tipo    |            Descripcion            |
|:---------:|:-----------:|:---------------------------------:|
|    scanner    |   scannerFile   | llegeix les entrades del document txt |

|  Funciones  |   Entradas   | Salidas |                                    Descripcion                                   |
|:---------:|:-----------:|:-----------:|:---------------------------------:|
|fromTeamFile |       -      |   void  | llegeix els equips del document txt i els afegeix a la llista teams    |
|fromMatchFile|       -      |   void  | llegeix els partits del document txt i els afegeix a la llista matches   |
|    from     |       -      |   void  | crida als dos mètodes anteriors         |
#### ToFile
|   Objeto  |     Tipo    |            Descripcion            |
|:---------:|:-----------:|:---------------------------------:|
|    teamPatch    |   Path   | path del document teamData.txt |
|    matchPatch    |   Path   | path del document matchData.txt |

|  Funciones  |   Entradas   | Salidas |                                    Descripcion                                   |
|:---------:|:-----------:|:-----------:|:---------------------------------:|
|toTeamFile |       League      |   void  | borra, crea y escribe el document teamData.txt de acuerdo al contenido de la lista teams del objeto League  |
|toMatchFile|       League      |   void  |  borra, crea y escribe el document matchData.txt de acuerdo al contenido de la lista match del objeto League  |
|    updateLeagueData     |       League      |   void  | crida los metodos toTeamFile y toMatchFile de esta clase         |

### Paquete Files
conté els documents de text que tenen les dades dels equips i els partits
#### teams
almacena datos de los equipos de una liga
##### estructura:
* No de equipos
* nombre abreviacion
#### matches
almacena datos de los partidos de una liga
##### estructura:
* No de partidos
* points 1 points2 team1 team2 



##Capturas de Pantalla
###Gestión de Datos(Insertar y Listar)
![meduDatos](./res/MenuDades1.png)
####Insertar Equipo:
![insertarEquipo](./res/introducirEquipo.png)
####Insertar Partido:
![insertarPartido](./res/introducirPartido.png)
####Listar Equipo:
![listaPartidos](./res/listaEquipos.png)
####Listar Partido:
![listaPartido](./res/listaPartidos.png)
###Estadísticas
¡[menuEstadisticas](./res/estadisticas.png)
####Globales
![globales](./res/estadisticasGlobales.png)
####Locales
![locales](./res/estadisticasLocales.png)
##Conclusiones
* Para un proyecto de java debe haber un correcta relacion con el equipo de trabajo
,pues sinó el proyecto tendrá diferentes formatos y discordancias
* Realizar un mapa de clases y objetos del proyecto facilita mucho la
    realizacion de la misma, pues es muy fácil llegar a conflictos entre clases
  
##Bibliografía
* Para hacer tablas en formato MarkDown:
  https://www.tablesgenerator.com/markdown_tables#
  

